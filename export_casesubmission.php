<?php
?>
<?php
ob_start();
session_start();
/*
* Export Mysql Data in excel or CSV format using PHP
* Downloaded from http://DevZone.co.in
*/

// Connect to database server and select database
 require_once 'common_files/includes/Constants.php';

// retrive data which you want to export
$query = "SELECT * FROM case_submission";
$header = '';
$data ='';
$export = mysqli_query ($bd,$query ) or die ( "Sql error : " );

// extract the field names for header 
$fields = mysqli_num_fields ( $export );

for ( $i = 0; $i < $fields; $i++ )
{
    $header .= mysqli_field_name( $export , $i ) . "\t";
}

// export data 
while( $row = mysqli_fetch_row( $export ) )
{
    $line = '';
    foreach( $row as $value )
    {                                            
        if ( ( !isset( $value ) ) || ( $value == "" ) )
        {
            $value = "\t";
        }
        else
        {
            $value = str_replace( '"' , '""' , $value );
            $value = '"' . $value . '"' . "\t";
        }
        $line .= $value;
    }
    $data .= trim( $line ) . "\n";
}
$data = str_replace( "\r" , "" , $data );

if ( $data == "" )
{
    $data = "\nNo Record(s) Found!\n";                        
}

// allow exported file to download forcefully
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Casesubmission_detail.xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";


?>
