<?php
session_start();
include_once("common_files/includes/Constants.php");
include_once("common_files/includes/rating.php");
if ($_SESSION["success"] == "" && $_SESSION["success"] != "success") {

  //  header('Location:login.php');
}
else {
	$caseId=$_GET['id'];
	$title=$_GET['title'];
	// echo "$caseId";
	// echo "$title";
	// exit('asdf');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>        
	<!-- META SECTION -->
	<title>CAD Conference | 2015</title>            
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />


	<!-- END META SECTION -->

	<!-- CSS INCLUDE -->        
	<link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
	<link href="css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="js/star-rating.min.js" type="text/javascript"></script>


	<!-- EOF CSS INCLUDE -->                                     
</head>
<body>
	<!-- START PAGE CONTAINER -->
	<div class="page-container">

		<!-- START PAGE SIDEBAR -->
		<div class="page-sidebar">
			<!-- START X-NAVIGATION -->
			<ul class="x-navigation">
				<li class="xn-logo">

					<a href="#" class="x-navigation-control"></a>
				</li>
				<li class="xn-profile">

					<div class="profile">
						<div class="profile-image">
							<img src="img/dp.png">
						</div>
						<div class="profile-data">
							<div class="profile-data-name"><?php echo $_SESSION['grader_name']; ?></div>
							<div class="profile-data-title">Grader</div>
						</div>
					</div>                                                                        
				</li>
				<li class="xn-title">Navigation</li>                    
				<li><a href="case_evaluation.php"><span class="fa fa-users"></span> Case Evaluation</a></li>                    
			</ul>
			<!-- END X-NAVIGATION -->
		</div>
		<!-- END PAGE SIDEBAR -->

		<!-- PAGE CONTENT -->
		<div class="page-content">

			<!-- START X-NAVIGATION VERTICAL -->
			<ul class="x-navigation x-navigation-horizontal x-navigation-panel">

				<li class="xn-icon-button pull-right">
					<a href="grader_logout.php" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
				</li> 

			</ul>

			<!-- PAGE TITLE -->
			<div class="page-title">                    
				<h2> <a href="case_evaluation.php"><span class="fa fa-arrow-circle-o-left"></span> Case Evaluation</a></h2>
			</div>
			<!-- END PAGE TITLE -->                

			<!-- PAGE CONTENT WRAPPER -->
			<div class="page-content-wrap">                

				<div class="row">
					<div class="col-md-12">

						<!-- START DEFAULT DATATABLE -->
						<div class="panel panel-default">
							<br><br>

							<div class="panel-body">
								<table class="table datatable">
									<thead>
										<tr>
											<th width="5%">Sr.No</th>
											<th width="45%">Case Name</th>
                                                <!-- <th width="15%">Presenter Name</th>
                                                	<th width="15%">Presenter Email</th> -->
                                                	<th width="10%">Rating Given</th>
                                                	<th width="10%">Downlaod</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            	<?php 

                                            	include_once("common_files/includes/Constants.php");
                                            	$cid=$_GET['id'];
                                            	$grader_id=$_SESSION['grader_id'];
                                            	$sqlQuery="select code,case_title,ppt,zip_location from case_submission where id=$cid";
                                            	$output=mysqli_query($bd,$sqlQuery);
                                            	$k=1;
                                            	while ($row=mysqli_fetch_assoc($output)) 

                                            	{ 

                                            		?>     

                                            		<tr>
                                            			<td><?php echo $k++; ?></td>
                                            			<td><?php echo $row['case_title']; ?></td>

                                            			<td> <input value="<?= getRatingByGraderId($grader_id,$cid); ?>" type="number" class="rating" min=0 max=5 step=0.1 data-size="md" data-stars="5" cid="<?= $cid; ?>" graderid= "<?= $grader_id; ?>" > </td>
                                            			<td>
                                            				<a href=<?php echo $row['zip_location']; ?>>
                                            					<button class="btn btn-info btn-block" >Download</button>
                                            				</a> 
                                            			</td>
                                            		</tr>
                                            		<?php 

                                            	} ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                                <!-- END DEFAULT DATATABLE -->

                                <!-- START SIMPLE DATATABLE -->

                                <!-- END SIMPLE DATATABLE -->

                            </div>
                        </div>                                

                    </div>
                    <!-- PAGE CONTENT WRAPPER -->                                
                </div>    
                <!-- END PAGE CONTENT -->
            </div>
            <!-- END PAGE CONTAINER -->       

            <!-- MESSAGE BOX-->
            <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            	<div class="mb-container">
            		<div class="mb-middle">
            			<div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            			<div class="mb-content">
            				<p>Are you sure you want to log out?</p>                    
            				<p>Press No if youwant to continue work. Press Yes to logout current user.</p>
            			</div>
            			<div class="mb-footer">
            				<div class="pull-right">
            					<a href="grader_logout.php" class="btn btn-success btn-lg">Yes</a>
            					<button class="btn btn-default btn-lg mb-control-close">No</button>
            				</div>
            			</div>
            		</div>
            	</div>
            </div>
            <!-- END MESSAGE BOX-->


            <script type="text/javascript">
            	$(function(){
            		$('.rating').on('rating.change', function(event, value, caption) {

            			cid = $(this).attr('cid');
            			graderid = $(this).attr('graderid');

            			if (confirm('Are you sure you want to save this rating?')) {
					    // Save it!
					    $.ajax({
					    	url: "common_files/includes/rating.php",
					    	dataType: "json",
					    	data: {rating:value, cid:cid, graderid:graderid, type:'save'},

					    	success: function( data ) {
					    		if(data.status == 'success'){
							        // alert("!");
							    }else if(data.status == 'error'){
							        // alert("Error on query!");
							    }
							},
							error: function(e) {
			                // Handle error here
			                // console.log(e);
			            },
			            timeout: 30000  
			        });

					} else {
					    // Do nothing!
					}

				});

            	});
            </script>


            <!-- START PRELOADS -->
            <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
            <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
            <!-- END PRELOADS -->                       

            <!-- START SCRIPTS -->
            <!-- START PLUGINS -->
            <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
            <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
            <!-- END PLUGINS -->                

            <!-- THIS PAGE PLUGINS -->
            <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
            <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

            <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- END PAGE PLUGINS -->

            <!-- START TEMPLATE -->


            <script type="text/javascript" src="js/plugins.js"></script>        
            <script type="text/javascript" src="js/actions.js"></script>        
            <!-- END TEMPLATE -->
            <!-- END SCRIPTS --> 

        </body>
        </html>