<?php   
session_start(); //to ensure you are using same session
unset($_SESSION["success"]);
session_destroy(); //destroy the session
header('location: grader_login.php'); //to redirect back to "index.php" after logging out
exit();
?>
