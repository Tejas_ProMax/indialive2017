<?php
include_once("Constants.php");

function connect() {
	// return 4.2;	
	$hostname = HST;
	$username = USR;
	$password = PWD;
	$dbname = DBN;
	$con = mysqli_connect($hostname, $username, $password, $dbname);	
	return $con;
}


//Gets rating if grader hasalready rated that case
function getRatingByGraderId($graderId,$cid) {
	$cid = str_replace(' ', '', $cid);
	
	$con = connect();
	
	$query = "SELECT rating as rating from rating WHERE gcid = $graderId AND cid = $cid";
	$result = mysqli_query($con, $query);

	if (!$result) {
		return 0;
	}

	$resultSet = mysqli_fetch_assoc($result);

	if(count($resultSet) > 0) {
		return $resultSet['rating'];
	} else {
		return 0;
	}
	
}

//if rating exists then update else insert
if(isset($_REQUEST['type'])) {
	if($_REQUEST['type'] == 'save') {

		$rating = $_REQUEST['rating'];
		$cid = $_REQUEST['cid'];
		$graderid = $_REQUEST['graderid'];
		$con = connect();
		$query = "SELECT ids FROM rating WHERE cid=$cid AND gcid=$graderid";

		$result = mysqli_query($con, $query);


		$ids = mysqli_fetch_array($result);
		if (isset($ids['ids'])) {
			$ids = $ids['ids'];
			$query = "UPDATE rating SET rating='$rating' WHERE ids=$ids";
		} else{
			$ids = $ids['ids'];
			
			$query = "INSERT INTO rating (cid, rating,gcid) VALUES ('$cid', $rating, '$graderid')";
			
		}


		$con = connect();


		$result = mysqli_query($con, $query);
		if (!$result)
			$response_array['status'] = 'error';
		else
			$response_array['status'] = 'success';

		return $response_array;
	} 
}

?>
