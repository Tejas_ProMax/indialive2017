<?php
session_start();
include_once("common_files/includes/Constants.php");
if ($_SESSION["success"] == "" && $_SESSION["success"] != "success") {

  //  header('Location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>        
    <!-- META SECTION -->
    <title>CAD Conference | 2015</title>            
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    
    <!-- END META SECTION -->
    
    <!-- CSS INCLUDE -->        
    <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
    <link href="css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/star-rating.min.js" type="text/javascript"></script>
    <!-- EOF CSS INCLUDE -->                                     
</head>
<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
        
        <!-- START PAGE SIDEBAR -->
        <div class="page-sidebar">
            <!-- START X-NAVIGATION -->
            <ul class="x-navigation">
                <li class="xn-logo">
                    
                    <a href="#" class="x-navigation-control"></a>
                </li>
                <li class="xn-profile">
                    
                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/dp.png">
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name">CAD Conference</div>
                            <div class="profile-data-title">Admin</div>
                        </div>
                    </div>                                                                        
                </li>
                <li class="xn-title">Navigation</li>                    
                <li><a href="case_submission.php"><span class="fa fa-image"></span> View Cases</a></li>
                <li><a href="grader.php"><span class="fa fa-user"></span> Manage Grader</a></li>
                <li><a href="assign_case.php"><span class="fa fa-users"></span> Assign Cases</a></li>
                <li><a href="case_report.php"><span class="fa fa-users"></span> View Records</a></li>                    
            </ul>
            <!-- END X-NAVIGATION -->
        </div>
        <!-- END PAGE SIDEBAR -->
        
        <!-- PAGE CONTENT -->
        <div class="page-content">
            
         <!-- START X-NAVIGATION VERTICAL -->
         <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
             
            <li class="xn-icon-button pull-right">
                <a href="logout.php" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
            </li> 
            
        </ul>
        
        <!-- PAGE TITLE -->
        <div class="page-title">                    
            <h2><span class="fa fa-arrow-circle-o-left"></span> Case Report</h2>
        </div>
        <!-- END PAGE TITLE -->                

        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">                
            
            <div class="row">
                <div class="col-md-12">

                    <!-- START DEFAULT DATATABLE -->
                    <div class="panel panel-default">
                        <br><br>

                        <div class="panel-body">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th width="10%">Sr.No</th>
                                        <th width="15%">Unique Code</th>
                                        <th width="60%">Case Name</th>
                                        <!-- <th width="20%">Presenter Name</th> -->
                                        <!-- <th width="15%">Presenter Email</th> -->
                                        <th width="15%">Rating Given</th>
                                    </tr>
                                </thead>
                                
                                <tbody>

                                   <?php 
                                   
                                   include_once("common_files/includes/Constants.php");

                                   $sqlQuery="select * from case_submission";
                                   $output=mysqli_query($bd,$sqlQuery);
                                   $num=mysqli_num_rows($output);
                                   $k=1;
                                   while ($row=mysqli_fetch_array($output)) 
                                    
                                    { ?>
                                       

                                        <tr>
                                            <td><?php echo $k++; ?></td>
                                            <td><?php echo $row['code']; ?></td>
                                            <td><?php echo $row['case_title']; ?></td>
                                            <!-- <td><?php //echo $row['fname'].' '.$row['lanme']; ?></td> -->
                                            <!-- <td><?php //echo $row['email']; ?></td> -->
                                            <?php 
                                            
                                            include_once("common_files/includes/Constants.php");
                                            $cid1=$row['id'];
                                            $sqlQuery10="SELECT AVG(rating) FROM rating group by (cid) having cid='$cid1'";
                                            $output10=mysqli_query($bd,$sqlQuery10);
                                            $num10=mysqli_fetch_array($output10);

                                            ?>

                                            <td>
                                                

                                             <?php echo $num10['AVG(rating)']; ?></td>
                                         </tr>
                                         <?php } ?>
                                     </tbody>
                                     
                                 </table>
                             </div>
                         </div>
                         <!-- END DEFAULT DATATABLE -->

                         <!-- START SIMPLE DATATABLE -->
                         
                         <!-- END SIMPLE DATATABLE -->

                     </div>
                 </div>                                
                 
             </div>
             <!-- PAGE CONTENT WRAPPER -->                                
         </div>    
         <!-- END PAGE CONTENT -->
     </div>
     <!-- END PAGE CONTAINER -->       
     
     <!-- MESSAGE BOX-->
     <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="logout.php" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- START PRELOADS -->
    <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
    <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
    <!-- END PRELOADS -->                       
    
    <!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
    <!-- END PLUGINS -->                

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
    
    <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    
    
    <script type="text/javascript" src="js/plugins.js"></script>        
    <script type="text/javascript" src="js/actions.js"></script>        
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 
    
</body>
</html>